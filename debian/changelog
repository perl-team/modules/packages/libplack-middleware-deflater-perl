libplack-middleware-deflater-perl (0.14-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * debian/*: update URLs from {search,www}.cpan.org to MetaCPAN.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Repository, Repository-
    Browse.
  * Add missing build dependency on libmodule-install-perl.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Submit.
  * Remove constraints unnecessary since buster
  * Bump debhelper from old 12 to 13.

  [ gregor herrmann ]
  * Update alternative test dependencies.
  * Import upstream version 0.14.
  * Update years of upstream copyright.
  * debian/copyright: drop stanza about removed files.
  * Update debian/upstream/metadata.
  * Refresh 0001-Fix-gzip-trailer-on-big-endian-hosts.patch.
  * Update build dependencies.
  * Declare compliance with Debian Policy 4.7.0.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Wed, 15 May 2024 11:49:48 +0200

libplack-middleware-deflater-perl (0.12-2) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Niko Tyni ]
  * Build-depend on libfurl-perl for improved test coverage
  * Add patch to fix gzip streams on big endian architectures.
    (Closes: #893472)
  * Update to debhelper compat level 10
  * Update to Standards-Version 4.1.3
  * Declare that the package does not need (fake)root to build
  * Add Testsuite declaration for autopkgtest-pkg-perl

 -- Niko Tyni <ntyni@debian.org>  Thu, 22 Mar 2018 17:21:44 +0200

libplack-middleware-deflater-perl (0.12-1) unstable; urgency=low

  * Import Upstream version 0.12

 -- Florian Schlichting <fsfs@debian.org>  Sat, 21 Sep 2013 00:48:09 +0200

libplack-middleware-deflater-perl (0.11-1) unstable; urgency=low

  * Imported Upstream version 0.11.
  * Removed copyright paragraphs for modules no longer shipped in inc/.
  * Updated build-dependencies.

 -- Florian Schlichting <fsfs@debian.org>  Wed, 07 Aug 2013 21:45:51 +0200

libplack-middleware-deflater-perl (0.09-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Florian Schlichting ]
  * Imported Upstream version 0.09
  * Email change: Florian Schlichting -> fsfs@debian.org
  * Bumped Standards-Version to 3.9.4 (no change)
  * Updated copyright years

 -- Florian Schlichting <fsfs@debian.org>  Sun, 26 May 2013 15:44:44 +0200

libplack-middleware-deflater-perl (0.08-1) unstable; urgency=low

  * Team upload.

  [ Florian Schlichting ]
  * Imported Upstream version 0.07

  [ gregor herrmann ]
  * New upstream release 0.08.

 -- gregor herrmann <gregoa@debian.org>  Mon, 25 Jun 2012 19:32:08 +0200

libplack-middleware-deflater-perl (0.06-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Correct Vcs-Browser and Vcs-Git URLs.

  [ Florian Schlichting ]
  * Imported Upstream version 0.06.
  * Bumped Standards-Version to 3.9.3 (use copyright-format 1.0).
  * Bumped years of upstream and Debian copyright.
  * Added copyright paragraphs for convenience copies of modules in inc/,
    besides already documented Module::Install.

 -- Florian Schlichting <fschlich@zedat.fu-berlin.de>  Sun, 03 Jun 2012 15:36:18 +0200

libplack-middleware-deflater-perl (0.05-1) unstable; urgency=low

  * Initial Release (Closes: #638989).

 -- Florian Schlichting <fschlich@zedat.fu-berlin.de>  Wed, 24 Aug 2011 17:06:18 +0000
